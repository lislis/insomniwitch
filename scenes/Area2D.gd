extends Area2D

signal hit

var can_click = false
var is_flickering = false

func _ready():
	randomize()
	$CollisionShape2D.disabled = false
	$StartTimer.wait_time = randi() % 3
	$GhostTimer.wait_time = 3 + (randi() % 5)
	$StartTimer.start()

func _process(delta):
	if is_flickering:
		$Bottle/AnimatedSprite.animation = 'flicker'
		$Bottle/AnimatedSprite.play()
	else:
		$Bottle/AnimatedSprite.animation = 'idle'
		$Bottle/AnimatedSprite.stop()

func _on_Grave_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if can_click:
			if $CollisionShape2D.disabled == false:
				print("Grave clicked")
				$CollisionShape2D.disabled = true
				emit_signal("hit")
		else:
			print("Wait for flickr")

func _on_GhostTimer_timeout():
	#print("ghost")
	$GhostTimer.wait_time = 3 + (randi() % 5)
	$FlickerTimer.start()
	can_click = true
	is_flickering = true

func _on_FlickerTimer_timeout():
	#print("flickr")
	$CollisionShape2D.disabled = false
	can_click = false
	is_flickering = false


func _on_StartTimer_timeout():
	$GhostTimer.start()
