extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var hits = 0 
var max_hits = 13

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	if hits == max_hits:
		get_tree().change_scene("res://scenes/graveyard.tscn")


func _on_Ghosti_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		hits += 1
